# FreeCodeCamp

## Projects & Certificantions

## 01. Responsive Web Design Certification (300 hours)

- [Build A Tribute Page](https://gitlab.com/talitastravassos/doctor-who-tribute) ✔
- [Build A Survey Form](https://gitlab.com/talitastravassos/survey-form) ✔
- [Build A Product Landing Page](https://gitlab.com/talitastravassos/headphone-landing-page) ✔
- [Build A Technical Documentation Page](https://gitlab.com/talitastravassos/technical-documentation-page) ✔
- Build A Personal Portfolio Webpage

### Certification


---

## 02. JavaScript Algorithms and Data Structures Certification (300 hours)

- Palindrome Checker
- Roman Numeral Converter
- Caesars Cipher
- Telephone Number Validator
- Cash Register

### Certification

---

## 03. Front End Libraries Certification (300 hours)

- Build A Random Quote Machine
- Build A Markdown Previewer
- Build A Drum Machine
- Build A Java Script Calculator
- Build A Pomodoro Clock

### Certification

---

## 04. Data Visualization Certification (300 hours)

- Visualize Data With A Bar Chart
- Visualize Data With A Scatterplot Graph
- Visualize Data With A Heat Map
- Visualize Data With A Choropleth Map
- Visualize Data With A Treemap Diagram

### Certification

---

## 05. Apis and Microservices Certification (300 hours)

- Timestamp Microservice
- Request Header Parser Microservice
- URL Shortener Microservice
- Exercise Tracker
- File Metadata Microservice

### Certification

---

## 06. Information Security and Quality Assurance Certification (300 hours)

- Metric Imperial Converter
- Issue Tracker
- Personal Library
- Stock Price Checker
- Anonymous Message Board

### Certification

---

## Full Stack Development Certification

Once you earn all 6 of these certifications, you'll be able to claim your freeCodeCamp Full Stack Development Certification. This final distinction signifies that you’ve completed around 1,800 hours of coding with a wide range of web development tools.

---

## 07. Coding Interview Prep (Thousands of hours of challenges)

- Algorithms - 09
- Data Structures - 47
- Take Home Projects - 20
- Rosetta Code - 87
- Project Euler - 480
